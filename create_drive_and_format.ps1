# This case come from an array of 4 disks in RAID 5. Windows only see a unique drive with the capacity of RAID 5 agregate. 
# When we launch this script, the disk already have a partition for Windows (C:) 
# We init the raw partition and format it in ReFS

# Init new partition creation. Auto allow a drive letter, generally "E:"
New-Partition -DiskNumber 0 -DriveLetter e -UseMaximumSize

# We format the volume to be seen and usable 
Format-Volume -DriveLetter e -FileSystem REFS