# Firefox
write-host "Install Mozilla in progress..."
E:\Sources\Firefox.exe -ms
write-host "Install of Mozilla done"


# Advanced ip scanner 
write-host "Install Advanced Ip Scanner in progress..."
E:\Sources\ipscan24.exe /SP- /VERYSILENT
write-host "Install of Advanced Ip Scanner done"

# TV_HOST.exe
write-host "Install TeamViewer in progress..."
E:\Sources\TV_HOST.exe /S
write-host "Install of TeamViewer done"

# efsw_nt64_fra.msi
write-host "Install Eset File Security in progress..."
E:\Sources\efsw_nt64_fra.msi /qn
write-host "Install of Eset File Security done"

pause