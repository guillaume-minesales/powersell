# Config
$Username = "bea"
$Password = "Bea123456!"
$List = @("Firefox.exe", "ipscan24.exe", "efsw_nt64_fra.msi","putty.exe", "TV_HOST.exe", "veeam95u4.iso", "winpower.exe")

# Creating download repo
New-item -Path "e:\" -Name "Sources" -ItemType "directory"


foreach ($i in $List){

    $LocalFile = "E:\Sources\$i"
    $RemoteFile = "ftp://172.28.1.98/$i"

    echo "Download $i in progress... ";
    
    # Create a FTPWebRequest
    $FTPRequest = [System.Net.FtpWebRequest]::Create($RemoteFile)
    $FTPRequest.Credentials = New-Object System.Net.NetworkCredential($Username,$Password)
    $FTPRequest.Method = [System.Net.WebRequestMethods+Ftp]::DownloadFile
    $FTPRequest.UseBinary = $true
    $FTPRequest.KeepAlive = $false

    # Send the ftp request
    $FTPResponse = $FTPRequest.GetResponse()

    # Get a download stream from the server response
    $ResponseStream = $FTPResponse.GetResponseStream()

    # Create the target file on the local system and the download buffer
    $LocalFileFile = New-Object IO.FileStream ($LocalFile,[IO.FileMode]::Create)
    [byte[]]$ReadBuffer = New-Object byte[] 1024

    # Loop through the download
    do {
    $ReadLength = $ResponseStream.Read($ReadBuffer,0,1024)
    $LocalFileFile.Write($ReadBuffer,0,$ReadLength)
    }
    while ($ReadLength -ne 0)

}